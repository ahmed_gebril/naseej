$(document).ready(function() {
    $('.carousel').carousel({
        direction: 'right'
    });

    $('.services .owl-carousel').owlCarousel({
        // loop: true,
        items: 4,
        margin: 30,
        rtl: true,
        nav: true,
        dots: false,
        navText: ["", ""],
        responsiveClass: true,
        responsive:{
            0:{
                items:1,
                nav:false,
                dots: true
            },
            520:{
                items:2,
                nav:false,
                dots: true
            },
            992:{
                items:3,
                nav:false,
                dots: true
            },
            1320:{
                items:4
            }
        }
    })
});
$(window).bind('scroll', function() {
    if($(window).scrollTop() > 10)
        $('header').addClass('fixed');
    else
        $('header').removeClass('fixed');
});